Quick-and-dirty project to test [ema](https://ema.srid.ca/).
The idea is to make some sort of blog with it.

For the site to display correctly, one must run with `-L md`
(e.g. `cabal run -- tb -L md` or `nix run -- -L md`).
This indicates the base directory for emanote.
