{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";

    emanote.url = "github:EmaApps/emanote";
    emanote.flake = false;
  };

  outputs = inputs: inputs.flake-utils.lib.eachDefaultSystem (system:
    with builtins;
    let
      pkgs = import inputs.nixpkgs { inherit system; };
      haskellDeps = drv: concatLists (attrValues drv.getCabalDeps);
      hs = pkgs.haskell.packages.ghc924.override (o: with pkgs.haskell.lib; {
        overrides = pkgs.lib.composeExtensions
          (o.overrides or (_: _: {}))
          (final: prev: rec {
            ema = dontCheck (final.callHackageDirect {
              pkg = "ema";
              ver = "0.8.0.0";
              sha256 = "sha256-ILH0UrS33Js4lDF1OtnPdKjr5Kf51Er3HWc8IGL91L8=";
            } {});
            relude = dontCheck (final.callHackage "relude" "1.1.0.0" {});

            # Emanote overrides:
            emanote = final.callCabal2nix "emanote" inputs.emanote {};
            generic-data = dontCheck prev.generic-data;
            heist-emanote = dontCheck (doJailbreak (unmarkBroken prev.heist-emanote));
            tailwind = addBuildTools (unmarkBroken prev.tailwind) [ tailwind' ];
          });
      });
      tailwind' = pkgs.nodePackages.tailwindcss.overrideAttrs (_: {
        plugins = [
          pkgs.nodePackages."@tailwindcss/aspect-ratio"
          pkgs.nodePackages."@tailwindcss/forms"
          pkgs.nodePackages."@tailwindcss/language-server"
          pkgs.nodePackages."@tailwindcss/line-clamp"
          pkgs.nodePackages."@tailwindcss/typography"
        ];
      });

      tb = hs.callCabal2nix "tb" ./. {};
    in
    {
      packages = {
        inherit tb;
        default = tb;
      };

      devShells.default = pkgs.mkShell {
        nativeBuildInputs = [
          (hs.ghcWithPackages (ps: haskellDeps tb))
          hs.cabal-install
          hs.haskell-language-server
          hs.hpack
          pkgs.clang
        ];
        shellHook = "if echo $- | grep -q i; then exec cabal run; fi";
      };
    }
  );
}
