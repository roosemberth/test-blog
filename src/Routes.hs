{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE UndecidableInstances #-}
module Routes where

import Data.Default (def)
import Data.Generics.Sum.Any (AsAny (_As))
import Data.Set (singleton)
import Data.Text.Encoding (encodeUtf8)
import Ema
import Ema.Route.Generic.TH
import Ema.Route.Lib.Extra.PandocRoute qualified as PR
import Ema.Route.Lib.Extra.StaticRoute qualified as SR
import GHC.Generics (Generic)
import Optics.Core ((%))
import Text.Blaze.Html.Renderer.Utf8 qualified as RU
import Text.Blaze.Html5 ((!))
import Text.Blaze.Html5 qualified as H
import Text.Blaze.Html5.Attributes qualified as A

data TbModel = TbModel
  { modelMd :: PR.Model
  , modelStatic :: SR.Model
  } deriving stock Generic

data HtmlRoute
  = HtmlRoute_Test
  deriving stock (Show, Eq, Ord, Generic, Enum, Bounded)

deriveGeneric ''HtmlRoute
deriveIsRoute ''HtmlRoute [t|
 '[ WithSubRoutes '[ FileRoute "test.html" ]
  ]|]

data TbRoute
  = Route_Html HtmlRoute
  | Route_Static (SR.StaticRoute "static")
  deriving stock (Eq, Show, Ord, Generic)

deriveGeneric ''TbRoute
deriveIsRoute ''TbRoute [t|
 '[ WithModel TbModel
  , WithSubRoutes '[HtmlRoute, SR.StaticRoute "static"]
  ]|]

instance EmaSite TbRoute where
  siteInput cliAct () = do
    mdModel <- siteInput @PR.PandocRoute cliAct $ def
      { PR.argBaseDir = "md"
      , PR.argFormats = singleton ".md"
      }
    staticModel <- siteInput @(SR.StaticRoute "static") cliAct ()
    pure $ TbModel <$> mdModel <*> staticModel
  siteOutput rp m = \case
    Route_Html r ->
      pure $ Ema.AssetGenerated Ema.Html $ RU.renderHtml $ do
        -- Describe the context where to embed the pandoc document using Blaze.
        H.docType
        H.html ! A.lang "en" $ do
          H.head $ pure ()
          H.body $ do
            H.main $ case r of
              HtmlRoute_Test -> do
                case PR.lookupPandocRoute (modelMd m) "test.md" of
                  Nothing -> error $ "Failed to lookup pandoc route: test.md"
                  Just (pandoc, render) ->
                    renderRawHtml $ PR.unPandocHtml (render pandoc)
    Route_Static r -> do
      siteOutput (rp % (_As @"Route_Static")) (modelStatic m) r
    where renderRawHtml = H.unsafeByteString . encodeUtf8
