{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE UndecidableInstances #-}
module TB.Route where

import Data.Generics.Sum.Any (AsAny (_As))
import Ema (EmaSite(..))
import Ema.Route.Generic.TH
import Emanote ()
import Emanote.Model qualified as EN
import Emanote.Route.SiteRoute qualified as EN
import Emanote.Source.Dynamic qualified as EN
import GHC.Generics (Generic)
import Optics.Optic ((%))

data Route
  = Route_Notes EN.SiteRoute
  deriving stock (Show, Eq, Ord, Generic)

data Model = Model
  { modelNotes :: EN.ModelEma
  } deriving stock Generic

data Config = Config
  { noteCfg :: EN.EmanoteConfig
  }

deriveGeneric ''Route
deriveIsRoute ''Route [t|
 '[ WithModel Model ]
  |]

instance EmaSite Route where
  type SiteArg Route = Config
  siteInput cliAct Config{..} = do
    modelNotes <- siteInput @EN.SiteRoute cliAct noteCfg
    pure $ Model <$> modelNotes
  siteOutput rp Model{..} = \case
    Route_Notes sr ->
      siteOutput @EN.SiteRoute (rp % (_As @"Route_Notes")) modelNotes sr
