{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE UndecidableInstances #-}
module Site where

import Control.Arrow (Arrow(..))
import Control.Monad (void)
import Ema qualified
import Emanote qualified as EN
import Emanote.CLI qualified as EN
import TB.Route qualified as TB

runTb :: IO ()
runTb = do
  (emCfg, cli) <- (EN.defaultEmanoteConfig &&& EN.emaCli) <$> EN.parseCli
  void $ Ema.runSiteWithCli @TB.Route cli (TB.Config emCfg)
